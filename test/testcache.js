const cacheFunction=require("../cache")

function cb(arg) {
    return arg; 
}

let compare = cacheFunction(cb); 

function test(func, input, expect) {
    let result = func(input); 
    console.log(result); 

    if(result === expect) {
        console.log('Test Passed');
    }else {
        console.log('Test Failed');
    }
}

test(compare, 2, 2);

