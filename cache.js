function cacheFunction(cb) {
    let obj = {}; 
    return (function(arg){
        if(Object.keys(obj).length === 0) {
            obj[arg] = cb(arg); 
        }
        return obj[arg]; 
    });
}
module.exports= cacheFunction;